# Instructions on usage
Clone this repo using the command
```
git clone https://gitlab.com/neil.astrologo/csv-translator.git
```

Then, place the .csv files you want to summarize into the import folder.

Afterwards, run using the command
```
python3 main.py
```

## NOTE
Optimizations can still be performed, this is the first working build.
