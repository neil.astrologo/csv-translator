import pandas as pd
import numpy as np
import sys
import os
from datetime import datetime


def extract_samplename(filename):
    x = filename.split("_")
    return x[2]

def append_csv(df, targ):
    # read target file
    targ_df = pd.read_csv(targ)

    # check if HotSpot ID columns match
    if(not targ_df["HotSpot ID"].equals(df["HotSpot ID"])):
        print("Mismatch in HotSpot IDs detected. Verify your data.")
        return 1

    # match, therefore, proceed
    # first, remove "HotSpot ID" from the new DF
    df_trim = df.loc[:, df.columns != "HotSpot ID"]

    # concatenate the source and new DF
    df_out = pd.concat([targ_df, df_trim], axis=1)
    df_out.to_csv(targ, index=False)
    return 0

def main(filename, out_file):
    # set import directory
    importdir = "../import/"
    outdir = "../out/"

    outdir_abs = os.path.abspath(outdir)

    if(not os.path.isdir(outdir)):
        print(f"Creating the directory {outdir_abs}")
        os.makedirs(outdir_abs)

    input_file = importdir + filename
    outdir = outdir + outfile
    sample_name = extract_samplename(filename)

    print(f"Summarizing data from sample {sample_name}...")

    df_src = pd.read_csv(input_file)

    # initialize destination dataframe
    # retrieve hotspot id and genotype
    df = df_src[["HotSpot ID", "Genotype"]].copy()


    # make mapping between genotype and read
    # gen_read = {"A" : "A Reads", "C" : "C Reads", "G":"G Reads", "T" : "T Reads"}

    # split genotype into the two member base pairs
    # example:
    # Genotype  g1  g2
    # CG        C   G
    # TA        T   A
    # GG        G   G
    g1 = df["Genotype"].str[0]
    g2 = df["Genotype"].str[1]

    # retrieve column values based on gen_read
    a1 = (g1 == "A") & (g2 != "A")
    a2 = (g1 != "A") & (g2 == "A")
    aa = (g1 == "A") & (g2 == "A")
    c1 = (g1 == "C") & (g2 != "C")
    c2 = (g1 != "C") & (g2 == "C")
    ca = (g1 == "C") & (g2 == "C")
    gg1 = (g1 == "G") & (g2 != "G")
    gg2 = (g1 != "G") & (g2 == "G")
    gga = (g1 == "G") & (g2 == "G")
    t1 = (g1 == "T") & (g2 != "T")
    t2 = (g1 != "T") & (g2 == "T")
    ta = (g1 == "T") & (g2 == "T")

    pd.set_option('display.max_rows', 500)
    
    # create read1 read2 dataframe based on a1, a2, aa, etc
    read = pd.DataFrame(0, index=np.arange(len(df_src)), columns=["Read1","Read2"])

    # A READS SECTION
    read["Read1"][a1] = df_src["A Reads"][a1]
    read["Read1"][aa] = df_src["A Reads"][aa]
    read["Read2"][a2] = df_src["A Reads"][a2]

    # C READS SECTION
    read["Read1"][c1] = df_src["C Reads"][c1]
    read["Read1"][ca] = df_src["C Reads"][ca]
    read["Read2"][c2] = df_src["C Reads"][c2]

    # G READS SECTION
    read["Read1"][gg1] = df_src["G Reads"][gg1]
    read["Read1"][gga] = df_src["G Reads"][gga]
    read["Read2"][gg2] = df_src["G Reads"][gg2]

    # T READS SECTION
    read["Read1"][t1] = df_src["T Reads"][t1]
    read["Read1"][ta] = df_src["T Reads"][ta]
    read["Read2"][t2] = df_src["T Reads"][t2]

    # concatenate read dataframe to df
    df_out = pd.concat([df, read], axis=1)

    # rename Genotype to samplename
    df_out.rename(columns={"Genotype":sample_name}, inplace=True)
    outdir_abs = os.path.abspath(outdir)
    print(f"Attempting to save to {outdir_abs}")

    # save to csv if csv does not yet exist
    if(not os.path.isfile(outdir)):
        print("File does not yet exist. Creating new file...")
        df_out.to_csv(outdir, index=False)
    else:
        print("File already exists. Appending...")
        if(not append_csv(df_out, outdir)):
            print("Appending successful.")
        else:
            print(f"Appending failed. Double check contents of {filename}")
            return 1

    return 0
        

if __name__ == "__main__":
    n = len(sys.argv)

    now = datetime.now()
    dt_string = now.strftime("%d-%m-%Y_%H-%M-%S")
    outfile = "compiled_" + dt_string + ".csv"

    if(n == 1):
        importdir  = "../import"
        importdir_abs = os.path.abspath(importdir)
        print(f"Compiling all data in the directory {importdir_abs}")

        filelist = os.listdir(importdir)
        for file in filelist:
            print(f"Processing {file}...")
            if(main(file, outfile) == 1):
                break
    else:
        for file in sys.argv[1:]:
            if(main(file, outfile) == 1):
                break
